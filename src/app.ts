import express from 'express'
import setupRoutes from './routes'
import setupMiddelwares from './middlewares'

const app = express()

setupMiddelwares(app)

setupRoutes(app)

export default app
