import { Router } from 'express'
import { adaptRoute } from '../adapters/express-routes.adapter'
import { makeBillController } from '../factories/make-bill-controller'
export default (router: Router): void => {
  router.post('/bills', adaptRoute(makeBillController()))
}
