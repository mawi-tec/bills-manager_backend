import { Controller } from '../adapters/controller'
import { BillsController } from '../controllers/bills-controller'
import { BillMongoRepository } from '../repositories/bills-mongo-repository'
export const makeBillController = (): Controller => {
  const handlersBill = new BillMongoRepository()
  return new BillsController(handlersBill)
}
