export default {
  mongoUrl:
    process.env.MONGO_URL ||
    'mongodb://sa:billsadm12@ds161860.mlab.com:61860/bills-manager',
  port: process.env.PORT || 5050
}
