import { BillsController } from './bills-controller'
import { Bill, AddBillModel } from '../interfaces/bill'
import MockDate from 'mockdate'

const makeFakeBill = (): AddBillModel => ({
  name: 'any_name',
  price: 150,
  created_at: new Date()
})

const makeHandlersBill = (): Bill => {
  class BillMongoRepositoryStub implements Bill {
    async add(data: AddBillModel): Promise<AddBillModel> {
      return new Promise(resolve => resolve(makeFakeBill()))
    }
  }
  return new BillMongoRepositoryStub()
}

interface SutTypes {
  sut: BillsController
  handlersBill: Bill
}

const makeSut = (): SutTypes => {
  const handlersBill = makeHandlersBill()
  const sut = new BillsController(handlersBill)
  return {
    sut,
    handlersBill
  }
}

describe('Bill Controller', () => {
  beforeAll(() => {
    MockDate.set(new Date())
  })

  beforeAll(() => {
    MockDate.reset()
  })

  it('Add Bill', async () => {
    const { sut } = makeSut()
    const response = await sut.handle({
      body: makeFakeBill()
    })
    expect(response.statusCode).toBe(200)
  })
})
