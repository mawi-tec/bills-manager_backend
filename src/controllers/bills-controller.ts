import { Controller } from '../adapters/controller'
import { ok, HttpRequest, HttpResponse, serverError } from '../protocols/http'
import { Bill } from '../interfaces/bill'
export class BillsController implements Controller {
  handlersBill: Bill;
  constructor(handlersBill: Bill) {
    this.handlersBill = handlersBill
  }

  async handle(httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const { name, price } = httpRequest.body
      await this.handlersBill.add({ name, price, created_at: new Date() })
      return ok(httpRequest.body)
    } catch (error) {
      console.log(httpRequest)
      return serverError(error)
    }
  }
}
