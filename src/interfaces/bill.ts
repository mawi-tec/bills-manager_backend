export interface AddBillModel {
  name: string
  price: number
  created_at: Date
}

export interface Bill {
  add(data: AddBillModel): Promise<AddBillModel>
}
