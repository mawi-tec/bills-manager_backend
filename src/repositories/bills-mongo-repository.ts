import { MongoHelper } from '../db/mongo-helper'
import { Bill, AddBillModel } from '../interfaces/bill'

export class BillMongoRepository implements Bill {
  async add(billData: AddBillModel): Promise<void> {
    const billCollection = await MongoHelper.getCollection('bills')
    await billCollection.insertOne(billData)
  }
}
